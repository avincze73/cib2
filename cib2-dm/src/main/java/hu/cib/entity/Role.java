package hu.cib.entity;

import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Role
 *
 */
@Entity

public class Role extends CibEntity {

	
	private String name;
	private static final long serialVersionUID = 1L;

	public Role() {
		super();
	}   
	public Role(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
   
}
