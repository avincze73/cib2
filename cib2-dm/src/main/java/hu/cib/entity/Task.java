package hu.cib.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Task
 *
 */
@Entity

public class Task extends CibEntity implements Serializable {

	@Basic(optional = false)
	@Column(name = "name")
	private String name;

	@ManyToOne(optional = false)
	@JoinColumn(name = "participantId", referencedColumnName = "id")
	private Employee participant;

	private static final long serialVersionUID = 1L;

	public Task() {
		super();
	}

	public Task(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Employee getParticipant() {
		return participant;
	}

	public void setParticipant(Employee participant) {
		this.participant = participant;
	}

	
}
