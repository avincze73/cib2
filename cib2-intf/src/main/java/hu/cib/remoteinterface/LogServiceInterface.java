package hu.cib.remoteinterface;


public interface LogServiceInterface {
    void log(String message);
}
