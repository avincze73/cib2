package hu.cib.message;

import java.io.Serializable;

public class AddDepartmentResponse implements Serializable {
    private String departmentId;

    public AddDepartmentResponse(String departmentId) {
        this.departmentId = departmentId;
    }

    public AddDepartmentResponse() {
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }
}
