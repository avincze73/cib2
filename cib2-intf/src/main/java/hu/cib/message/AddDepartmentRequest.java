package hu.cib.message;


import java.io.Serializable;

public class AddDepartmentRequest implements Serializable{
    private String departmentName;

    public AddDepartmentRequest(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
}
