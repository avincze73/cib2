package hu.cib.message;

import java.io.Serializable;

public class AddProjectRequest implements Serializable {
    private String projectName;

    public AddProjectRequest(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
