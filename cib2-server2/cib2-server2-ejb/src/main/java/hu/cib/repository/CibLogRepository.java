package hu.cib.repository;

import hu.cib.entity.CibLog;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class CibLogRepository {
    @PersistenceContext(unitName = "cib2-server1-pu3")
    private EntityManager em;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(CibLog entity) {
        em.merge(entity);
    }
}