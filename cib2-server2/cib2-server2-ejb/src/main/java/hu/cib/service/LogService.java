/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.cib.service;

import hu.cib.entity.CibLog;
import hu.cib.remoteinterface.LogServiceInterface;
import hu.cib.repository.CibLogRepository;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 *
 * @author martin
 */
@Stateless
@Remote(LogServiceInterface.class)
public class LogService implements LogServiceInterface {
    @EJB
    private CibLogRepository cibLogRepository;


    @Override
    public void log(String message) {
        //System.out.println("itt vagyok");
        CibLog cibLog = new CibLog();
        cibLog.setMessage(message);
        cibLogRepository.save(cibLog);
    }
}
