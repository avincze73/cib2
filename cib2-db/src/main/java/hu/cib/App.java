package hu.cib;

import hu.cib.entity.*;
import hu.cib.util.SecurityUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        Logger.getLogger("LogInterceptor").log(Level.INFO, " is called.");
        init("cib-db-pu1");
        init("cib-db-pu2");
        init2("cib-db-pu3");
    }

    private static void init2(String persistenceUnitName){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(persistenceUnitName);
        factory.close();
    }


    private static void init(String persistenceUnitName) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory(persistenceUnitName);
        EntityManager em = factory.createEntityManager();
        EntityTransaction tr = em.getTransaction();
        tr.begin();

        Department department = new Department("department1");
        em.persist(department);
        Department department2 = new Department("department2");
        em.persist(department2);
        Role role1 = new Role("admin");
        em.persist(role1);
        Role role2 = new Role("developer");
        em.persist(role2);

        Employee employee = new Employee();
        employee.setFirstName("firstname1");
        employee.setLastName("lastname1");
        employee.setUserName("username1");
        employee.setDepartment(department);
        employee.setAddress(new Address("zip1", "city1", "street1"));
        employee.setRoleList(new HashSet<Role>());
        employee.getRoleList().add(role1);
        employee.getRoleList().add(role2);
        employee.setPassword(SecurityUtil.create().getPasswordHash("password1"));
        em.persist(employee);

        Employee employee2 = new Employee();
        employee2.setFirstName("firstname2");
        employee2.setLastName("lastname2");
        employee2.setUserName("username2");
        employee2.setDepartment(department2);
        employee2.setRoleList(new HashSet<Role>());
        employee2.getRoleList().add(role1);
        employee2.getRoleList().add(role2);
        employee2.setPassword(SecurityUtil.create().getPasswordHash("password2"));
        em.persist(employee2);

        Project project = new Project();
        project.setName("project1");
        project.setStartDate(new Date());
        project.setEndDate(new Date(project.getStartDate().getTime() + 7 * 24 * 60 * 60 * 1000));
        project.setOwner(employee);
        project.setTaskList(new ArrayList<Task>());

        Task task = new Task("task1");
        task.setParticipant(employee);
        project.getTaskList().add(task);
        task = new Task("task11");
        task.setParticipant(employee2);
        project.getTaskList().add(task);

        em.persist(project);

        Project project2 = new Project();
        project2.setName("project2");
        project2.setStartDate(new Date());
        project2.setEndDate(new Date(project2.getStartDate().getTime() + 14 * 24 * 60 * 60 * 1000));
        project2.setOwner(employee2);
        project2.setTaskList(new ArrayList<Task>());

        Task task2 = new Task("task2");
        task2.setParticipant(employee);
        project2.getTaskList().add(task2);
        task2 = new Task("task22");
        task2.setParticipant(employee);
        project2.getTaskList().add(task2);

        em.persist(project2);
        //employee.setBoss(employee2);
        //employee2.setBoss(employee);
        //tr.commit();
        factory.close();
    }
}
