/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.cib.rs;

import hu.cib.entity.Department;
import hu.cib.facade.DepartmentServiceFacade;
import hu.cib.message.AddDepartmentRequest;
import hu.cib.message.AddDepartmentResponse;
import hu.cib.sessionservice.DepartmentServices;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

/**
 * REST Web Service
 *
 * @author avincze
 */
@Path("department")
@RequestScoped
public class DepartmentRsServices {

    @EJB
    private DepartmentServiceFacade departmentServiceFacade;

    @EJB
    private DepartmentServices departmentServices;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of DepartmentRsServices
     */
    public DepartmentRsServices() {
    }


    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDepartment(@PathParam("id") String id){
        return Response.ok(departmentServices.get(Long.parseLong(id))).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDepartments(){
        return Response.ok(departmentServices.getAll()).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addDepartment(AddDepartmentRequest request){
        AddDepartmentResponse response = departmentServiceFacade.addDepartment(request);
        URI bookUri = context.getAbsolutePathBuilder().path(response.getDepartmentId()).build();
        return Response.created(bookUri).build();
    }

    /**
     * Retrieves representation of an instance of com.cib.DepartmentRsServices
     * @return an instance of java.lang.String
     */
    //@GET
    //@Produces(MediaType.TEXT_PLAIN)
    //public String getXml() {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
      //  return "aaa";
    //}

    /**
     * PUT method for updating or creating an instance of DepartmentRsServices
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
}
