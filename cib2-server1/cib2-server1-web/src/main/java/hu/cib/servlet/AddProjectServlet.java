package hu.cib.servlet;

import hu.cib.entity.Department;
import hu.cib.entity.Project;
import hu.cib.exception.CibException;
import hu.cib.facade.ProjectServiceFacade;
import hu.cib.message.AddProjectRequest;
import hu.cib.sessionservice.DepartmentServices;
import hu.cib.sessionservice.ProjectService;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddProjectServlet", urlPatterns = {"/addprojectservlet"})
public class AddProjectServlet extends HttpServlet {

    @Inject
    //private ProjectService projectService;
    private ProjectServiceFacade projectServiceFacade;


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Project project = new Project();
        //project.setName("project3");

        projectServiceFacade.addProject(new AddProjectRequest("project12"));
    }
}
