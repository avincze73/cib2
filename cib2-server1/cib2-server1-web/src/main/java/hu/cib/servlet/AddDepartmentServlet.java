package hu.cib.servlet;

import hu.cib.entity.Department;
import hu.cib.exception.CibException;
import hu.cib.facade.DepartmentServiceFacade;
import hu.cib.message.AddDepartmentRequest;
import hu.cib.sessionservice.DepartmentServices;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddDepartmentServlet", urlPatterns = {"/adddepartmentservlet"})
public class AddDepartmentServlet extends HttpServlet {

    @EJB
    //private DepartmentServices departmentServices;
    private DepartmentServiceFacade departmentServiceFacade;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Department department = new Department();
        department.setName("department11");
        //try {
        departmentServiceFacade.addDepartment(new AddDepartmentRequest("department12"));
        //departmentServices.addDepartment(department);
        //} catch (CibException e) {
        //    e.printStackTrace();
        //}
    }
}
