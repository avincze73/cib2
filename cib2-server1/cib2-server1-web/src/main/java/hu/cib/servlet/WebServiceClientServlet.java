package hu.cib.servlet;

import hu.cib.entity.Department;
import hu.cib.facade.DepartmentServiceFacade;
import hu.cib.message.AddDepartmentRequest;
import hu.cib.sessionservice.WebServiceClientEjb;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "WebServiceClientServlet", urlPatterns = {"/webserviceclientservlet"})
public class WebServiceClientServlet extends HttpServlet {

    @EJB
    //private DepartmentServices departmentServices;
    private WebServiceClientEjb webServiceClientEjb;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        webServiceClientEjb.callRemoteWebService();
    }
}
