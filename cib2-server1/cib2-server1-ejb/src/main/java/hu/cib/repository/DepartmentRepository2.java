package hu.cib.repository;

import hu.cib.entity.Department;
import hu.cib.exception.CibException;
import hu.cib.interceptor.CibLoggerInterceptor;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@LocalBean
@Interceptors(CibLoggerInterceptor.class)
public class DepartmentRepository2 {

    @PersistenceContext(unitName = "cib2-server1-pu2")
    private EntityManager em;


    @Resource
    private SessionContext sessionContext;

    public void save(Department department)  throws CibException {
        //throw new CibException("error occured");
        em.merge(department);
        //sessionContext.setRollbackOnly();
    }

    public List<Department> findAll(){
        return em.createQuery("select d from Department d", Department.class).getResultList();
    }
}
