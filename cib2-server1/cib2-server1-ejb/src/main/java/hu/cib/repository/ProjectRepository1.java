package hu.cib.repository;

import hu.cib.entity.Project;
import hu.cib.interceptor.CibLoggerInterceptor;
import hu.cib.producer.Cib1Database;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional(Transactional.TxType.REQUIRED)
@Interceptors(CibLoggerInterceptor.class)
public class ProjectRepository1 {

    @Inject
    @Cib1Database
    private EntityManager em;

    public void save(Project project) {
        em.merge(project);
    }
}
