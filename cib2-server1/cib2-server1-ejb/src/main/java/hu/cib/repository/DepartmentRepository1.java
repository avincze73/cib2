package hu.cib.repository;

import hu.cib.entity.Department;
import hu.cib.exception.CibException;
import hu.cib.interceptor.CibLoggerInterceptor;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@LocalBean
@Interceptors(CibLoggerInterceptor.class)
public class DepartmentRepository1 {

    @PersistenceContext(unitName = "cib2-server1-pu1")
    private EntityManager em;


    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(Department department) throws CibException {
        em.merge(department);
    }

    public List<Department> findAll(){
        return em.createQuery("select d from Department d", Department.class).getResultList();
    }

    public Department get(Long id){
        return em.find(Department.class, id);
    }
}
