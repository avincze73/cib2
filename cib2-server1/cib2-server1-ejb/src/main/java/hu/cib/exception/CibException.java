package hu.cib.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class CibException extends Exception {
    public CibException(String message) {
        super(message);
    }
}
