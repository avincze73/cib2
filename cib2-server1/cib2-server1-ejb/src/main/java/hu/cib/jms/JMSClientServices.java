package hu.cib.jms;

import hu.cib.interceptor.CibLoggerInterceptor;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
@Interceptors(CibLoggerInterceptor.class)
public class JMSClientServices {

    @Inject
    @JMSConnectionFactory( "jms/libertyQCF")
    private JMSContext jmsContext;

    @Resource(lookup = "jndi_INPUT_Q1")
    private Queue queue1;

    @Resource(lookup = "jndi_INPUT_Q2")
    private Queue queue2;

    @Resource(lookup = "jndi_INPUT_Q3")
    private Queue queue3;

    public void sendMessageToQueue() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Sending third message to jms queue");
        String text = "third message from jms client";
        jmsContext.createProducer().send(queue1, text);
        jmsContext.createProducer().send(queue2, text);
        jmsContext.createProducer().send(queue3, text);
    }
}
