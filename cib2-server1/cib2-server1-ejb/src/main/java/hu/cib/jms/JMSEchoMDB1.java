package hu.cib.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.logging.Level;
import java.util.logging.Logger;

@MessageDriven(
        name = "JMSEchoMDB1",
        activationConfig = {
                @ActivationConfigProperty(propertyName = "destinationType",
                        propertyValue = "javax.jms.Queue"),
                @ActivationConfigProperty(propertyName = "destination",
                        propertyValue = "jndi_INPUT_Q1")
        }
)
public class JMSEchoMDB1 implements MessageListener {

    @Override
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void onMessage(Message message) {
        TextMessage textMsg = (TextMessage) message;
        try {
            String text = textMsg.getText();
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Received message 1: " + text);
        } catch (JMSException e) {
            e.printStackTrace();
        }


    }
}
