package hu.cib.jms;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.logging.Level;
import java.util.logging.Logger;

@MessageDriven(
        name = "JMSEchoMDB3",
        activationConfig = {
                @ActivationConfigProperty(propertyName = "destinationType",
                        propertyValue = "javax.jms.Queue"),
                @ActivationConfigProperty(propertyName = "destination",
                        propertyValue = "jndi_INPUT_Q3")
        }
)
public class JMSEchoMDB3 implements MessageListener {

    @Override
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void onMessage(Message message) {
        TextMessage textMsg = (TextMessage) message;
        try {
            String text = textMsg.getText();
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Received message 3: " + text);
        } catch (JMSException e) {
            e.printStackTrace();
        }


    }
}
