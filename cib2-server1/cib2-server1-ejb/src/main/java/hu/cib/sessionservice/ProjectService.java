package hu.cib.sessionservice;

import hu.cib.entity.Project;
import hu.cib.interceptor.CibLoggerInterceptor;
import hu.cib.repository.ProjectRepository1;
import hu.cib.repository.ProjectRepository2;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.Transactional;

@Transactional(Transactional.TxType.REQUIRED)
@Interceptors(CibLoggerInterceptor.class)
public class ProjectService {

    @Inject
    private ProjectRepository1 projectRepository1;

    @Inject
    private ProjectRepository2 projectRepository2;

    public void addProject(Project project){
        projectRepository1.save(project);
        projectRepository2.save(project);
    }
}
