package hu.cib.sessionservice;

import hu.cib.jms.JMSClientServices;
import hu.cib.jms.JMSEchoMDB1;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
@LocalBean
public class ScheduledServices {

    @EJB
    private JMSClientServices jmsClientServices;

    @Schedule(second = "30", minute = "0/1", hour = "*", info = "ScheduledServices.echoToLog timer", persistent = false)
    public void echoToLog() {
        jmsClientServices.sendMessageToQueue();
    }

}
