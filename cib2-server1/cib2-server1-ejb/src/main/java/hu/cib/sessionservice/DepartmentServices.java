package hu.cib.sessionservice;

import hu.cib.entity.Department;
import hu.cib.exception.CibException;
import hu.cib.interceptor.CibLoggerInterceptor;
import hu.cib.remoteinterface.LogServiceInterface;
import hu.cib.repository.DepartmentRepository1;
import hu.cib.repository.DepartmentRepository2;

import javax.ejb.*;
import javax.interceptor.Interceptors;
import java.util.List;

@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
@Interceptors(CibLoggerInterceptor.class)
public class DepartmentServices {


    @EJB
    private DepartmentRepository1 departmentRepository1;

    @EJB
    private DepartmentRepository2  departmentRepository2;

    //@EJB(lookup = "java:global/cib2-server2-ear-1.0-SNAPSHOT/cib2-server2-ejb-1.0-SNAPSHOT/LogService!hu.cib.remoteinterface.LogServiceInterface")
    //private LogServiceInterface logService;


    public void addDepartment(Department department) throws CibException {
        departmentRepository1.save(department);
        departmentRepository2.save(department);
        //logService.log("hello");
    }

    public Department get(Long id){
        return departmentRepository1.get(id);
    }

    public List<Department> getAll(){
        return departmentRepository1.findAll();
    }
}
