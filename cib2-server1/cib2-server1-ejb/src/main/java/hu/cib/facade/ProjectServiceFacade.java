package hu.cib.facade;

import hu.cib.entity.Project;
import hu.cib.interceptor.CibLoggerInterceptor;
import hu.cib.message.AddDepartmentRequest;
import hu.cib.message.AddDepartmentResponse;
import hu.cib.message.AddProjectRequest;
import hu.cib.message.AddProjectResponse;
import hu.cib.sessionservice.ProjectService;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.transaction.Transactional;

@Transactional(Transactional.TxType.REQUIRED)
@Interceptors(CibLoggerInterceptor.class)
public class ProjectServiceFacade {

    @Inject
    private ProjectService projectService;

    public AddProjectResponse addProject(AddProjectRequest request) {
        AddProjectResponse response = new AddProjectResponse();
        Project project = new Project();
        project.setName(request.getProjectName());
        projectService.addProject(project);
        return response;
    }

}
