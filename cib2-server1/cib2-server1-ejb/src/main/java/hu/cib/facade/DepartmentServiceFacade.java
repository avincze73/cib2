package hu.cib.facade;

import hu.cib.entity.Department;
import hu.cib.exception.CibException;
import hu.cib.interceptor.CibLoggerInterceptor;
import hu.cib.message.AddDepartmentRequest;
import hu.cib.message.AddDepartmentResponse;
import hu.cib.sessionservice.DepartmentServices;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

@Stateless
@LocalBean
@Interceptors(CibLoggerInterceptor.class)
public class DepartmentServiceFacade {

    @Resource
    private SessionContext context;
    @EJB
    private DepartmentServices departmentServices;
    public AddDepartmentResponse addDepartment(AddDepartmentRequest request){
        AddDepartmentResponse response = new AddDepartmentResponse();
        Department department = new Department();
        department.setName(request.getDepartmentName());
        try {
            departmentServices.addDepartment(department);
            response.setDepartmentId(department.getId().toString());
        } catch (CibException e) {
            e.printStackTrace();
        }
        return response;
    }

}
