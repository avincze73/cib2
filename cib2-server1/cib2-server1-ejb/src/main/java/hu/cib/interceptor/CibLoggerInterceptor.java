/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.cib.interceptor;

import hu.cib.remoteinterface.LogServiceInterface;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author avincze
 */
public class CibLoggerInterceptor {

    @EJB(lookup = "java:global/cib2-server2-ear-1.0-SNAPSHOT/cib2-server2-ejb-1.0-SNAPSHOT/LogService!hu.cib.remoteinterface.LogServiceInterface")
    private LogServiceInterface logService;

    @Resource
    private SessionContext context;

    @AroundInvoke
    public Object incerceptBusinessMethod(InvocationContext ctx) throws Exception {
        Logger.getLogger(getClass().getName()).log(Level.INFO,
                ctx.getMethod().getDeclaringClass().getCanonicalName()
                        + "::" + ctx.getMethod().getName() + " is called.");
        logService.log(ctx.getMethod().getDeclaringClass().getCanonicalName()
                + "::" + ctx.getMethod().getName() + " is called by " + context.getCallerPrincipal().getName());
        LocalTime start = LocalTime.now();
        Object ret = ctx.proceed();
        LocalTime end = LocalTime.now();
        Logger.getLogger(getClass().getName()).log(Level.INFO,
                "Time: " +  (end.getNano() - start.getNano()) + " ns");

        return ret;
    }
}
